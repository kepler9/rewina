<?php

namespace frontend\models;
use backend\models\Applicants;
use Yii;
use yii\base\Model;
use common\models\User;
use backend\models\Methods;

/**
 * Signup form
 */
class SignupForm extends Model
{
    public $username;
    public $firstname;
    public $lastname;
    public $phone;
    public $email;
    public $password;


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname','lastname'],'string'],
            [['firstname','lastname','phone','password','email'],'required'],

            ['phone', 'trim'],
            ['phone', 'string', 'min' => 9, 'max' => 13],

            ['email', 'trim'],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'This email address has already been taken.'],

            ['password', 'string', 'min' => Yii::$app->params['user.passwordMinLength']],
        ];
    }

    /**
     * Signs user up.
     *
     * @return bool whether the creating new account was successful and email was sent
     */
    public function signup() {
        
        if (!$this->validate()) {
            return null;
        }
        
        $user = new User();
        $user->firstname = $this->firstname;
        $user->lastname = $this->lastname;
        $user->email = $this->email;
        $user->phone = mb_substr($this->phone, -9);
        $user->setPassword($this->password);
        $user->generateAuthKey();
        $user->generateEmailVerificationToken();
        if($user->save()){
            $applicant = new Applicants();
            $applicant->user_id = $user->user_id;
            if($applicant->save()){
                return $this->sendEmail($user);
            }
        } 
        return $this->redirect(['site/signup']);       
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */
    protected function sendEmail($user)
    {
        $methods = new Methods();
        $settings = $methods->get_system_settings();
        return Yii::$app
            ->mailer
            ->compose(
                ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
                ['user' => $user]
            )
            ->setFrom([$settings->sender_email => $settings->sender_name])
            ->setTo($this->email)
            ->setSubject('Account Registration at ' . $settings->app_name)
            ->send();
    }
}
