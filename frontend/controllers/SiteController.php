<?php

namespace frontend\controllers;

use frontend\models\ResendVerificationEmailForm;
use frontend\models\VerifyEmailForm;
use Yii;
use yii\base\InvalidArgumentException;
use yii\web\BadRequestHttpException;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use frontend\models\PasswordResetRequestForm;
use frontend\models\ResetPasswordForm;
use frontend\models\SignupForm;
use frontend\models\ContactForm;
use backend\models\ApplicationsSearch;
use backend\models\Applications;
use backend\models\JobsSearch;
use backend\models\Jobs;
use backend\models\FormFields;
use backend\models\FormFieldsData;
use yii\web\UploadedFile;
use backend\models\FileUpload;
use backend\models\Applicants;
use backend\models\Users;

/**
** Site controller
**/

class SiteController extends Controller
{
    /**
    ** {@inheritdoc}
    **/

    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout', 'signup','apply','profile','update'],
                'rules' => [
                    [
                        'actions' => ['signup'],
                        'allow' => true,
                        'roles' => ['?'],
                    ],
                    [
                        'actions' => ['logout','apply','profile','update'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new JobsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);
        return $this->render('index',['dataProvider'=>$dataProvider,'searchModel'=>$searchModel]);
    }

    /**
     * Logs in a user.
     *
     * @return mixed
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }        

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        }

        $model->password = '';

        return $this->render('login', [
            'model' => $model,
        ]);
    }

    /**
     * Logs out the current user.
     *
     * @return mixed
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->goHome();
    }

    /**
     * Displays contact page.
     *
     * @return mixed
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail(Yii::$app->params['adminEmail'])) {
                Yii::$app->session->setFlash('success', 'Thank you for contacting us. We will respond to you as soon as possible.');
            } else {
                Yii::$app->session->setFlash('error', 'There was an error sending your message.');
            }

            return $this->refresh();
        }

        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return mixed
     */
    public function actionAbout()
    {
        return $this->render('about');
    }

    /**
     * Signs user up.
     *
     * @return mixed
     */
    public function actionSignup()
    {
        $model = new SignupForm();
        if ($model->load(Yii::$app->request->post()) && $model->signup()) {
            Yii::$app->session->setFlash('success', 'Thank you for registration. Please check your inbox for verification email.');
            return $this->redirect(['login']);
        }

        return $this->render('signup', [
            'model' => $model,
        ]);
    }

    /**
     * Requests password reset.
     *
     * @return mixed
     */
    public function actionRequestPasswordReset()
    {
        $model = new PasswordResetRequestForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');

                return $this->goHome();
            }

            Yii::$app->session->setFlash('error', 'Sorry, we are unable to reset password for the provided email address.');
        }

        return $this->render('requestPasswordResetToken', [
            'model' => $model,
        ]);
    }

    /**
     * Resets password.
     *
     * @param string $token
     * @return mixed
     * @throws BadRequestHttpException
    */

    public function actionResetPassword($token) {
        try {
            $model = new ResetPasswordForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }

        if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
            Yii::$app->session->setFlash('success', 'New password saved.');

            return $this->goHome();
        }

        return $this->render('resetPassword', [
            'model' => $model,
        ]);
    }

    /**
     * Verify email address
     *
     * @param string $token
     * @throws BadRequestHttpException
     * @return yii\web\Response
     */
    
    public function actionVerifyEmail($token) {
        try {
            $model = new VerifyEmailForm($token);
        } catch (InvalidArgumentException $e) {
            throw new BadRequestHttpException($e->getMessage());
        }
        if (($user = $model->verifyEmail()) && Yii::$app->user->login($user)) {
            Yii::$app->session->setFlash('success', 'Your email has been confirmed!');
            return $this->goHome();
        }

        Yii::$app->session->setFlash('error', 'Sorry, we are unable to verify your account with provided token.');
        return $this->goHome();
    }

    /**
     * Resend verification email
     *
     * @return mixed
    */

    public function actionResendVerificationEmail() {
        $model = new ResendVerificationEmailForm();
        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            if ($model->sendEmail()) {
                Yii::$app->session->setFlash('success', 'Check your email for further instructions.');
                return $this->goHome();
            }
            Yii::$app->session->setFlash('error', 'Sorry, we are unable to resend verification email for the provided email address.');
        }

        return $this->render('resendVerificationEmail', [
            'model' => $model
        ]);
    }

    public function actionJob($id) {
        $model = Jobs::findOne(['status'=>1,'job_id'=>$id]);
        $application = new Applications();
        if(Yii::$app->user->identity){
           $applicant = Applicants::findOne(['user_id'=>Yii::$app->user->identity->user_id]);
            $application = Applications::findOne(['applicant_id'=>$applicant->applicant_id,'job_id'=>$id]);
            if(!$application){
                $application = new Applications();
            } 
        }
        
        if($model) {
            return $this->render('job',[
                'model'=>$model,
                'application'=>$application
            ]);
        }
    }

    public function actionApply($id) {
        $model = Jobs::findOne(['status'=>1,'job_id'=>$id]);
        if($model) {
            $applicant = Applicants::findOne(['user_id'=>Yii::$app->user->identity->user_id]);
            $application = Applications::findOne(['applicant_id'=>$applicant->applicant_id,'job_id'=>$id]);
            if(!$application){
                $application = new Applications();
            }
            $FormFields = FormFields::find()->where(['job_id'=>$id,'Step'=>1])->all();
            if(Yii::$app->request->post()) {
                $application->load(Yii::$app->request->post());
                $application->job_id = $id;
                $application->applicant_id = $applicant->applicant_id;
                $application->status_id = 1;
                
                $FileUpload = new FileUpload();
                $FileUpload->load(Yii::$app->request->post(),"");
                $FileUpload->UploadFile = UploadedFile::getInstanceByName("cv");
                $application->cv = $FileUpload->upload();

                $FileUpload->UploadFile = UploadedFile::getInstanceByName("cover_letter");
                $application->cover_letter = $FileUpload->upload();
                
                if($application->save()) {
                    $this->saveData([
                        'FormFields'=>$FormFields,
                        'job_id'=>$id,
                        'application_id'=>$application->application_id
                    ]);
                    return $this->redirect(['profile','submitted'=>1]);
                }
            }else{
                return $this->render('apply',[
                    'model'=>$model,
                    'application'=>$application,
                    'FormFields'=>$FormFields
                ]);
            }
        }
    }

    public function saveData($params){
        $FormFields = $params['FormFields'];
        $data = Yii::$app->request->post();
        $job_id = $params['job_id'];
        $application_id = $params['application_id'];
        foreach($FormFields as $FormField){
            if(isset($data[$FormField->FormFieldID]) || isset($_FILES[$FormField->FormFieldID])){
                $FormFieldsData = FormFieldsData::findOne(['Submitted'=>1,'user_id'=>Yii::$app->user->identity->user_id,'application_id'=>$application_id,'FormFieldID'=>$FormField->FormFieldID]);
                if(!$FormFieldsData){
                    $FormFieldsData = new FormFieldsData();
                    $FormFieldsData->user_id = Yii::$app->user->identity->user_id;
                    $FormFieldsData->FormFieldID = $FormField->FormFieldID;
                    $FormFieldsData->application_id = $application_id;
                    $FormFieldsData->job_id = $params['job_id'];
                    $FormFieldsData->Submitted = 1;
                }
                $FormFieldsData->Name = $FormField->Name;
                if($FormField->Type == "file") {
                    $FileUpload = new \backend\models\FileUpload();
                    $FileUpload->load(Yii::$app->request->post(),"");
                    $FileUpload->UploadFile = UploadedFile::getInstanceByName("".$FormField->FormFieldID."");
                    $FormFieldsData->Value = $FileUpload->upload();
                }else{
                    $FormFieldsData->Value = $data[$FormField->FormFieldID];
                }
                $FormFieldsData->DateCreated = date('Y-m-d H:i:s');
                $FormFieldsData->save();
            }                
        }
        return;
    }
    public function actionProfile($submitted=null){
        if($submitted) {
            Yii::$app->session->setFlash('success', 'Your job application has been received');
        }
        $applicant = Applicants::findOne(['user_id'=>Yii::$app->user->identity->user_id]);
        $searchModel = new ApplicationsSearch();
        $searchModel->applicant_id = $applicant->applicant_id;
        $applications = $searchModel->search($this->request->queryParams);
        return $this->render('profile',[
            'applications'=>$applications
        ]);
    }
    public function actionUpdate(){
        if(Yii::$app->request->post()){
            $user = Users::findOne(Yii::$app->user->identity->user_id);
            $user->load(Yii::$app->request->post());
            $user->phone = mb_substr($user->phone, -9);
            $FileUpload = new FileUpload();
            $FileUpload->load(Yii::$app->request->post(),"");
            $FileUpload->UploadFile = UploadedFile::getInstanceByName("avatar");
            $avatar = $FileUpload->upload();
            if($avatar){
                $user->avatar = $avatar;
            }
            if($user->save()){
                return $this->redirect(['profile']);
            }
        }else{
            return $this->render('update');
        }
    }
}
