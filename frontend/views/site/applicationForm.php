<?php if($FormFields): foreach($FormFields as $Field): ?>
	<?php $Required = $Field->Required ? "required" : NULL; ?>
	<!-- textbox -->
	<?php if($Field->Type == 'textbox'):?>
		<div class="form-group">
			<label><?=$Field->Label?></label>
			<input data-validation="<?=$Field->Validation." ".$Required?>" placeholder="" name="<?=$Field->FormFieldID?>" class="form-control">
		</div>
	<?php endif;?>
	<!-- date -->
	<?php if($Field->Type == 'date'):?>
		<div class="form-group">
			<label><?=$Field->Label?></label>
			<input data-validation="<?=$Field->Validation." ".$Required?>" placeholder="" type="date" name="<?=$Field->FormFieldID?>" class="form-control">
		</div>
	<?php endif;?>
	<!-- textarea -->
	<?php if($Field->Type == 'textarea'):?>
		<div class="form-group">
			<label><?=$Field->Label?></label>
			<textarea data-validation="<?=$Field->Validation." ".$Required?>" placeholder="" name="<?=$Field->FormFieldID?>" class="form-control"></textarea>
		</div>
	<?php endif;?>
	<!-- dropdown -->
	<?php if($Field->Type == 'dropdown'):?>
		<?php $options = explode('|',$Field->Options);?>
		<div class="form-group">
			<label><?=$Field->Label?></label>
			<select data-validation="<?=$Field->Validation." ".$Required?>" name="<?=$Field->FormFieldID?>" class="form-control">
				<option value="">Select</option>
				<?php if($options): foreach($options as $option):?>
					<?php $parts = explode(':', $option); ?>
					<option value="<?=trim($parts[0])?>"><?=trim($parts[1])?></option>
				<?php endforeach;endif;?>
			</select>
		</div>
	<?php endif;?>
	<!-- file -->
	<?php if($Field->Type == 'file'):?>
		<div class="form-group">
			<label><?=$Field->Label?></label><br>
			<input accept=".pdf,.png,.jpeg,.jpg,.gif" type="file" data-validation="<?=$Field->Validation." ".$Required?>" placeholder="" name="<?=$Field->FormFieldID?>">
		</div>
	<?php endif;?>
<?php endforeach;endif;?>