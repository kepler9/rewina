<?php
	use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\grid\ActionColumn;
    use yii\grid\GridView;
	$this->title = 'My Profile';
	\yii\web\YiiAsset::register($this);
?>
<div class="row">
	<div class="col-md-3">
		<div class="boxed">
			<?=$this->render('summary');?>
		</div>
		<br>
	</div>
	<div class="col-md-9">
		<div class="boxed table-responsive">
			 <?= GridView::widget([
                'dataProvider' => $applications,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    ['attribute'=>'job.title','label'=>'Job'],
                    ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                        return "<span data-toggle='tooltip' data-trigger='hover' data-placement='top' data-original-title='".$model->status->description."' class='badge badge-".$model->status->alert."'>".$model->status->name."</span>";
                    },'label'=>'Status'],
                    ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                        return $model->status_description ? $model->status_description : $model->status->description;
                    },'label'=>'Description'],

                    'date_applied:date',
                ],
            ]); ?>
		</div>
	</div>
</div>