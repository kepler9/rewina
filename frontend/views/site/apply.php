<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->title;
\yii\web\YiiAsset::register($this);
use yii\widgets\ActiveForm;
?>
<div class="row">
	<div class="col-md-4">
		<div class="boxed">
			<?= DetailView::widget([
                'model' => $model,
                'attributes' => [
                    ['attribute'=>'title','label'=>'Job'],
                    'deadline:date',
                ],
            ]) ?>
		</div>
		<br>
	</div>
	<div class="col-md-8">
		<div class="boxed">
			<?php $form = ActiveForm::begin();?>
				<?=$this->render('applicationForm',['FormFields' => $FormFields]);?>
				<label>Upload CV</label>
				<?=$form->field($application,'cv')->fileInput(['name'=>'cv'])->label(false);?>
				<hr class="hr-20">
				<label>Upload Cover Letter</label>
				<?=$form->field($application,'cover_letter')->fileInput(['name'=>'cover_letter'])->label(false);?>
				<hr class="hr-20">
				<div class="form-group float-right">
	                <?= Html::submitButton('Submit', ['class' => 'btn btn-primary btn-sm']) ?>
	            </div>
	            <span class="clearfix"></span>
			<?php ActiveForm::end();?>
		</div>
	</div>
</div>