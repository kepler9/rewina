<?php
	$user = \Yii::$app->user->identity;
	use yii\helpers\Html;
?>
<center>
	<div class="profile-picture">
		<?=Html::img('/uploads/'.$user->avatar,['width'=>'80']);?>
	</div>
	<hr class="hr-20">
	<?=$user->firstname.' '.$user->lastname.' '.$user->surname;?>
	<hr class="hr-20">
	<?=Html::a('Update',['site/update'],['class'=>'btn btn-primary btn-sm col-md-12']);?>
</center>