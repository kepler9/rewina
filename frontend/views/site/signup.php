<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
$this->title = 'Register';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 offset-3">
    <div class="site-signup boxed">
        <h5><?= Html::encode($this->title) ?></h5>
        <hr class="hr-20">
        <p>Please fill out the following fields to signup:</p>
        <?php $form = ActiveForm::begin(['id' => 'form-signup']); ?>
            <?= $form->field($model, 'firstname')->textInput() ?>
            <?= $form->field($model, 'lastname')->textInput() ?>
            <?= $form->field($model, 'email') ?>
            <?= $form->field($model, 'phone') ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <div class="form-group float-right">
                <?= Html::submitButton('Signup', ['class' => 'btn btn-primary btn-sm', 'name' => 'signup-button']) ?>
            </div>
            <span class="clearfix"></span>
        <?php ActiveForm::end(); ?>
    </div>
</div>
