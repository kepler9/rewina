<?php
use yii\bootstrap4\Html;
use yii\bootstrap4\ActiveForm;
$this->title = 'Reset Password';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 offset-lg-3">
    <div class="site-reset-password boxed">
        <h5><?= Html::encode($this->title) ?></h5>
        <hr class="hr-20">
        <p>Please choose your new password:</p>
        <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
            <?= $form->field($model, 'password')->passwordInput(['autofocus' => false]) ?>
            <div class="form-group float-right">
                <?= Html::submitButton('Save', ['class' => 'btn btn-primary btn-sm']) ?>
            </div>
            <span class="clearfix"></span>
        <?php ActiveForm::end(); ?>
    </div>
</div>
