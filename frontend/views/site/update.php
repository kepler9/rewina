<?php
	use yii\helpers\Html;
	use yii\widgets\DetailView;
	$this->title = 'Update Profile';
	\yii\web\YiiAsset::register($this);
	$user = \Yii::$app->user->identity;
	use yii\widgets\ActiveForm;
?>
<div class="row">
	<div class="col-md-3">
		<div class="boxed">
			<?=$this->render('summary',['user'=>$user]);?>
		</div>
		<br>
	</div>
	<div class="col-md-9">
		<div class="boxed">
			<?php $form = ActiveForm::begin(); ?>
				<?=$form->field($user,'surname');?>
				<?=$form->field($user,'firstname');?>
				<?=$form->field($user,'lastname');?>
				<?=$form->field($user,'phone');?>
				<label>Profile Picture</label>
				<?=$form->field($user,'avatar')->fileInput(['name'=>'avatar'])->label(false);?>
				<hr class="hr-20">
				<div class="form-group float-right">
					<button class="btn btn-success btn-sm">Save</button>
				</div>
				<span class="clearfix"></span>
			<?php ActiveForm::end(); ?>
		</div>
	</div>
</div>