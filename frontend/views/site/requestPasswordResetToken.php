<?php
    use yii\bootstrap4\Html;
    use yii\bootstrap4\ActiveForm;
    $this->title = 'Request Password Reset';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 offset-lg-3">
    <div class="site-request-password-reset boxed">
        <h5><?= Html::encode($this->title) ?></h5>
        <hr class="hr-20">
        <p>Please fill out your email. A link to reset password will be sent there.</p>
        <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
            <?= $form->field($model, 'email')->textInput(['autofocus' => false]) ?>
            <div class="form-group float-right">
                <?= Html::submitButton('Send', ['class' => 'btn btn-primary btn-sm']) ?>
            </div>
            <span class="clearfix"></span>
        <?php ActiveForm::end(); ?>
    </div>
</div>