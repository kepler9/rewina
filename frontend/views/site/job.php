<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Jobs', ['/'], ['class' => 'btn btn-secondary btn-sm text-muted']) ?>
                        <?= !$application->application_id ? Html::a('Apply', ['apply', 'id' => $model->job_id], ['class' => 'btn btn-primary btn-sm']) : NULL ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="jobs-view">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'title',
                        ['attribute'=>'company.name','label'=>'Company'],
                        ['attribute'=>'industry.name','label'=>'Industry'],
                        'description:html',
                        'requirements:html',
                        'responsibilities:html',
                        'seniority',
                        ['attribute'=>'country.nicename','label'=>'Country'],
                        'deadline:date',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>