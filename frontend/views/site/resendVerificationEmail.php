<?php
    use yii\bootstrap4\Html;
    use yii\bootstrap4\ActiveForm;
    $this->title = 'Resend verification email';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 offset-lg-3">
    <div class="site-resend-verification-email boxed">
        <h5><?= Html::encode($this->title) ?></h5>
        <hr class="hr-20">
        <p>Please fill out your email. A verification email will be sent there.</p>
        <?php $form = ActiveForm::begin(['id' => 'resend-verification-email-form']); ?>
        <?= $form->field($model, 'email')->textInput(['autofocus' => false]) ?>
        <div class="form-group float-right">
            <?= Html::submitButton('Send', ['class' => 'btn btn-primary btn-sm']) ?>
        </div>
        <span class="clearfix"></span>
        <?php ActiveForm::end(); ?>
    </div>
</div>
