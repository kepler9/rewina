<?php
$this->title = Yii::$app->name;
use yii\widgets\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
?>
<div class="row">
	<div class="col-md-3">
		<div class="boxed">
			<div class="jobs-search">

			    <?php $form = ActiveForm::begin(['action' => ['index'],'method' => 'get']); ?>

			    <?= $form->field($searchModel, 'title')->textInput(['placeholder'=>'Search by title'])->label('Job') ?>

			    <?=$form->field($searchModel,'industry_id')->widget(Select2::classname(), 
			        [
			            'data' => ArrayHelper::map(backend\models\Industries::find()->all(),'industry_id','name'),
			            'options' => ['placeholder' => 'Select'],
			            'pluginOptions' => [
			                'allowClear' => true
			            ],
			        ]
			    )->label('Industry'); ?>

			    <?=$form->field($searchModel,'country_id')->widget(Select2::classname(), 
			        [
			            'data' => ArrayHelper::map(backend\models\Countries::find()->all(),'country_id','nicename'),
			            'options' => ['placeholder' => 'Select'],
			            'pluginOptions' => [
			                'allowClear' => true
			            ],
			        ]
			    )->label('Country'); ?>

			    <div class="form-group">
			        <?= Html::submitButton('Search', ['class' => 'btn btn-primary btn-sm col-md-12']) ?>
			    </div>

			    <?php ActiveForm::end(); ?>

			</div>
		</div>
		<br>
	</div>
	<div class="col-md-9">
		<div class="boxed table-responsive">
			<?= GridView::widget([
	            'dataProvider' => $dataProvider,
	            // 'filterModel' => $searchModel,
	            'columns' => [
	                ['class' => 'yii\grid\SerialColumn'],

	                ['attribute'=>'title','label'=>'Job','format'=>'raw','value'=>function($model){
	                	return Html::a($model->title,['site/job','id'=>$model->job_id],['style'=>'color:#03172D;font-weight:500']);
	                }],
	                ['attribute'=>'company.name','label'=>'Company'],
	                ['attribute'=>'country.nicename','label'=>'Country'],
	                'deadline:date',
	                [
	                    'class' => ActionColumn::className(),
	                    'urlCreator' => function ($action, backend\models\Jobs $model, $key, $index, $column) {
	                        return Url::toRoute(['job', 'id' => $model->job_id]);
	                    },
	                    'template' => '{view}'
	                ],
	            ],
	        ]); ?>
	    </div>
	</div>
</div>