<?php
    use yii\bootstrap4\Html;
    use yii\bootstrap4\ActiveForm;
    $this->title = 'Login';
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-lg-6 offset-lg-3">
    <div class="site-login boxed">
        <h5><?= Html::encode($this->title) ?></h5>
        <hr class="hr-20">
        <p>Please fill out the following fields to login:</p>
        <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
            <?= $form->field($model, 'email')->textInput() ?>
            <?= $form->field($model, 'password')->passwordInput() ?>
            <?= $form->field($model, 'rememberMe')->checkbox() ?>
            <div style="color:#999;margin:1em 0">
                Don't have an account? <?= Html::a('Register', ['site/signup']) ?>
                <br>
                Forgot your password? <?= Html::a('Reset', ['site/request-password-reset']) ?>
                <br>
                New verification email? <?= Html::a('Resend', ['site/resend-verification-email']) ?>
            </div>
            <hr class="hr-20">
            <div class="form-group float-right">
                <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-sm', 'name' => 'login-button']) ?>
            </div>
            <span class="clearfix"></span>
        <?php ActiveForm::end(); ?>
    </div>
</div>