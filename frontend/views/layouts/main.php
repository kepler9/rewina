<?php
    use common\widgets\Alert;
    use frontend\assets\AppAsset;
    use yii\bootstrap4\Breadcrumbs;
    use yii\bootstrap4\Html;
    use yii\bootstrap4\Nav;
    use yii\bootstrap4\NavBar;
    use backend\models\Methods;
    AppAsset::register($this);
    $method = new Methods();
    $settings = $method->get_system_settings();
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>" class="h-100">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body class="d-flex flex-column h-100">
<?php $this->beginBody() ?>

<header>
    <?php
    NavBar::begin([
        'brandLabel' => Html::img('/images/logo.png',['width'=>60]),
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-light bg-light fixed-top shadow',
        ],
    ]);
    $menuItems = [
        ['label' => 'Jobs', 'url' => ['/site/index']],
    ];
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => 'Register', 'url' => ['/site/signup']];
        $menuItems[] = ['label' => 'Login', 'url' => ['/site/login']];
    } else {
        $menuItems[] = ['label' => 'Profile', 'url' => ['/site/profile']];
        $menuItems[] = '<li>'
            . Html::beginForm(['/site/logout'], 'post', ['class' => 'form-inline'])
            . Html::submitButton(
                'Logout (' . Yii::$app->user->identity->firstname . ')',
                ['class' => 'btn btn-link logout']
            )
            . Html::endForm()
            . '</li>';
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>
</header>

<main role="main" class="flex-shrink-0">
    <div class="container">
        <div class="content">
            <div class="col-md-6 offset-md-3">
                <?= Alert::widget() ?>
            </div>
            <span class="clearfix"></span>
            <?= $content ?>
        </div>
    </div>
</main>

<footer class="footer bg--dark mt-auto py-3 text-muted">
    <div class="container">
        <p class="float-left">&copy; <?= date('Y') ?> <b><?= Html::encode($settings->app_name) ?></b>. All Rights Reserved.</p>
    </div>
</footer>

<?php $this->endBody() ?>
<script type="text/javascript">
    $.validate({
        lang: 'en'
    });
</script>
</body>
</html>
<?php $this->endPage();
