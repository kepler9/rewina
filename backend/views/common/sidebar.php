<?php
  use yii\helpers\Html;
?>
<div class="bg-theme border-right" id="sidebar-wrapper">
  <div class="sidebar-heading">
    <center>
      <?=Html::img('/images/rewina-logo-white.png',['width'=>78])?>
      <h5></h5>
    </center>
  </div>
  <ul class="list-group list-group-flush list-unstyled sidebar-menu">
    <li><a href="/welcome/index" class="list-group-item list-group-item-action"><i class="fa fa-home"></i> Home</a></li>
    <li><a href="/jobs/index" class="list-group-item list-group-item-action"><i class="fa fa-briefcase"></i> Jobs</a></li>
    <li><a href="/companies/index" class="list-group-item list-group-item-action"><i class="fa fa-building"></i> Companies</a></li>
    <li><a href="/embassies/index" class="list-group-item list-group-item-action"><i class="fa fa-globe"></i> Embassies</a></li>
    <li><a href="/ministries/index" class="list-group-item list-group-item-action"><i class="fa fa-globe"></i> Ministries</a></li>
    <li><a href="/applicants/index" class="list-group-item list-group-item-action"><i class="fa fa-user"></i> Applicants</a></li>
    <li><a href="/applications/index" class="list-group-item list-group-item-action"><i class="fa fa-circle-o-notch"></i> Applications</a></li>
    <?php //if(Yii::$app->user->identity->role == 'admin'): ?>
      <li>
        <a href="#settings-menu" data-toggle="collapse" class="list-group-item list-group-item-action"><i class="fa fa-wrench"></i> Settings <span class="fa fa-angle-down"></span></a>
        <ul id="settings-menu" class="collapse list-group list-group-flush sub-sidebar-menu list-unstyled">
          <li><a href="/admins/index" class="list-group-item list-group-item-action"> Admins</a></li>
          <li><a href="/industries/index" class="list-group-item list-group-item-action"> Industries</a></li>
          <li><a href="/form-fields/index" class="list-group-item list-group-item-action"> Application Form </a></li>
          <li><a href="/application-status/index" class="list-group-item list-group-item-action"> Application Status </a></li>
          <li><a href="/mail-queue/index" class="list-group-item list-group-item-action"> Mail Queue </a></li>
          <li><a href="/settings/index" class="list-group-item list-group-item-action"> System </a></li>
        </ul>
      </li>
    <?php //endif;?>
  </ul>
</div>