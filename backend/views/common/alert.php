<?php if(Yii::$app->session->hasFlash('msg')):?>
<div class="alert alert-success">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <?=ucwords(Yii::$app->session->getFlash('msg'));?>
</div>
<?php endif;?>
<?php if(Yii::$app->session->hasFlash('error_msg')):?>
<div class="alert alert-danger">
    <button type="button" class="close" data-dismiss="alert" aria-hidden="true"></button>
    <?=ucwords(Yii::$app->session->getFlash('error_msg'));?>
</div>
<?php endif;?>