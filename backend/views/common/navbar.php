<?php
  use yii\helpers\Html;
?>
<nav class="navbar navbar-sibtc navbar-expand-lg">
  <a href="#" id="menu-toggle">
    <?=Html::img('/images/toggle.png',['width'=>30]);?>
  </a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
  <?=Html::img('/images/menu.png',['width'=>30]);?>
  </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=Html::img('/images/523-Profile-Avatar.png',['width'=>30,'style'=>'position:relative;top:0px;'])?> </a>
        <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
          <a class="dropdown-item" href="/">Home</a>
          <div class="dropdown-divider"></div>
          <a class="dropdown-item" href="/users/view">Profile</a>
          <div class="dropdown-divider"></div>
          <?= Html::a('Logout',['/site/logout'],['data-method' => 'post','class'=>'dropdown-item']);?>
        </div>
      </li>
    </ul>
  </div>
</nav>