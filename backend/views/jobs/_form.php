<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
use mihaildev\ckeditor\CKEditor;
?>

<div class="jobs-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?=$form->field($model,'industry_id')->widget(Select2::classname(), 
        [
            'data' => ArrayHelper::map(backend\models\Industries::find()->all(),'industry_id','name'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    )->label('Industry'); ?>

    <?=$form->field($model,'company_id')->widget(Select2::classname(), 
        [
            'data' => ArrayHelper::map(backend\models\Companies::find()->all(),'company_id','name'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    )->label('Company'); ?>

    <?= $form->field($model, 'description')->widget(CKEditor::className(),['editorOptions'=>['preset'=>'basic']]) ?>

    <?= $form->field($model, 'requirements')->widget(CKEditor::className(),['editorOptions'=>['preset'=>'standard']]) ?>

    <?= $form->field($model, 'responsibilities')->widget(CKEditor::className(),['editorOptions'=>['preset'=>'standard'],'options'=>['rows'=>2]]) ?>

    <?= $form->field($model, 'seniority')->dropDownList(['Entry and Basic-level'=>'Entry and Basic-level','Mid-level'=>'Mid-level','Senior-level'=>'Senior-level'],['prompt'=>'Select']) ?>

    <?=$form->field($model,'country_id')->widget(Select2::classname(), 
        [
            'data' => ArrayHelper::map(backend\models\Countries::find()->all(),'country_id','nicename'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    )->label('Country'); ?>

    <?= $form->field($model, 'deadline')->textInput(['type'=>'date'])->label('Application Deadline') ?>

    <?= $form->field($model, 'status')->dropDownList([1=>'Published',0=>'Inactive'],['prompt'=>'Select']) ?>

    <?= $form->field($model, 'positions')->textInput()->label('Open Positions') ?>

    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>
