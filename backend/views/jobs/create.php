<?php
use yii\helpers\Html;
$this->title = 'Create Jobs';
$this->params['breadcrumbs'][] = ['label' => 'Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
	<div class="boxed">
		<div class="page-header bg-light">
			<div class="float-left">
				<h5><?=$this->title?></h5>
			</div>
			<div class="float-right">
				<ul class="list-inline">
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
				</ul>
			</div>
			<span class="clearfix"></span>
		</div>
		<div class="page-content">
			<div class="jobs-create">

			    <?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>

			</div>
		</div>
	</div>
</div>