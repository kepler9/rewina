<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$this->title = 'Jobs';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Create Jobs', ['create'], ['class' => 'btn btn-success']) ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="jobs-index table-responsive">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'title',
                        ['attribute'=>'industry.name','label'=>'Industry'],
                        'seniority',
                        ['attribute'=>'country.nicename','label'=>'Country'],
                        'deadline:date',
                        'date_created:date',
                        ['attribute'=>'status','value'=>function($model){
                            return $model->status ? '<span class="badge badge-success">published</span>' : '<span class="badge badge-danger">inactive</span>';
                        },'format'=>'raw'],
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, backend\models\Jobs $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'job_id' => $model->job_id]);
                            },
                            'template' => '{view}'
                        ],
                    ],
                ]); ?>


            </div>
        </div>
    </div>
</div>