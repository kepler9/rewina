<?php
use yii\helpers\Html;
$this->title = 'Update Job: ' . $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Jobs', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'job_id' => $model->job_id]];
$this->params['breadcrumbs'][] = 'Update';
$model->deadline = $model->deadline ? date('Y-m-d',strtotime($model->deadline)) : NULL;
?>

<div class="page-wrapper">
	<div class="boxed">
		<div class="page-header bg-light">
			<div class="float-left">
				<h5><?=$this->title?></h5>
			</div>
			<div class="float-right">
				<ul class="list-inline">
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
				</ul>
			</div>
			<span class="clearfix"></span>
		</div>
		<div class="page-content">
			<div class="jobs-update">

			    <?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>

			</div>
		</div>
	</div>
</div>