<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$this->title = 'Companies';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Add Company', ['create'], ['class' => 'btn btn-success']) ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="embassies-index table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'name',
                        ['attribute'=>'country.nicename','label'=>'Country'],
                        ['attribute'=>'user_id','label'=>'Admin','value'=>function($model){
                            return $model->user->firstname." ".$model->user->lastname;
                        }],
                        'user.email:email',
                        'user.phone',
                        ['attribute'=>'status','value'=>function($model){
                            return $model->status ? '<span class="badge badge-success">active</span>' : '<span class="badge badge-danger">inactive</span>';
                        },'format'=>'raw'],
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, backend\models\Companies $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'company_id' => $model->company_id]);
                            },
                            'template'=>'{view}',
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>