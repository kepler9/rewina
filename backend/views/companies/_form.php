<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use yii\helpers\ArrayHelper;
?>

<div class="embassies-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($user, 'firstname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($user, 'lastname')->textInput(['maxlength' => true]) ?>

    <?= $form->field($user, 'email')->textInput(['maxlength' => true]) ?>

    <?= $form->field($user, 'phone')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true])->label('Company Name') ?>

    <?=$form->field($model,'country_id')->widget(Select2::classname(), 
        [
            'data' => ArrayHelper::map(backend\models\Countries::find()->all(),'country_id','nicename'),
            'options' => ['placeholder' => 'Select'],
            'pluginOptions' => [
                'allowClear' => true
            ],
        ]
    )->label('Country'); ?>

    <?= $form->field($model, 'status')->dropDownList([1=>'Active',0=>'Inactive']) ?>

    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>
