<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\MailQueueSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="mail-queue-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'id') ?>

    <?= $form->field($model, 'sender_name') ?>

    <?= $form->field($model, 'sender_email') ?>

    <?= $form->field($model, 'email_to') ?>

    <?= $form->field($model, 'cc') ?>

    <?php // echo $form->field($model, 'bcc') ?>

    <?php // echo $form->field($model, 'subject') ?>

    <?php // echo $form->field($model, 'html_body') ?>

    <?php // echo $form->field($model, 'text_body') ?>

    <?php // echo $form->field($model, 'reply_to') ?>

    <?php // echo $form->field($model, 'charset') ?>

    <?php // echo $form->field($model, 'created_at') ?>

    <?php // echo $form->field($model, 'attempts') ?>

    <?php // echo $form->field($model, 'last_attempt_time') ?>

    <?php // echo $form->field($model, 'sent_time') ?>

    <?php // echo $form->field($model, 'time_to_send') ?>

    <?php // echo $form->field($model, 'swift_message') ?>

    <?php // echo $form->field($model, 'attachments') ?>

    <?php // echo $form->field($model, 'sent') ?>

    <?php // echo $form->field($model, 'status') ?>

    <?php // echo $form->field($model, 'email_template') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
