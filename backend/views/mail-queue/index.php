<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Mail Queues';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-wrapper">
    <div class="boxed">
        <?php if(Yii::$app->user->identity):?>
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item"><?=Html::a('',['#']);?></li>
                    <li class="list-inline-item"><?=Html::a('Clear',['clear'],['class'=>'btn btn-danger']);?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <?php endif;?>
        <div class="page-content table-responsive">
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                // 'filterModel' => $searchModel,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],

                    'email_to:email',
                    'subject',
                    'html_body:raw',
                    'status',
                    'created_at',

                    ['class' => 'yii\grid\ActionColumn','template'=>'{delete}'],
                ],
            ]); ?>
        </div>
    </div>
</div>