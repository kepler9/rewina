<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->user->firstname." ".$model->user->lastname;
$this->params['breadcrumbs'][] = ['label' => 'Applicants', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Update', ['update', 'applicant_id' => $model->applicant_id], ['class' => 'btn btn-primary']) ?>
                    </li>
                    <li class="list-inline-item">
                        <?= Html::a('Delete', ['delete', 'applicant_id' => $model->applicant_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="applicants-view">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                       'user.firstname',
                        'user.lastname',
                        'user.surname',
                        'user.email:email',
                        'user.phone',
                        ['attribute'=>'status','value'=>function($model){
                            return $model->status ? '<span class="badge badge-success">active</span>' : '<span class="badge badge-danger">inactive</span>';
                        },'format'=>'raw'],
                        ['attribute'=>'user.created_at','label'=>'Date','format'=>'date'],
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>