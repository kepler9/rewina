<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="applicants-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model->user, 'surname')->textInput() ?>
    <?= $form->field($model->user, 'firstname')->textInput() ?>
    <?= $form->field($model->user, 'lastname')->textInput() ?>
    <?= $form->field($model->user, 'email')->textInput() ?>
    <?= $form->field($model->user, 'phone')->textInput() ?>
    <?= $form->field($model, 'status')->dropDownList([1=>'Active',0=>'Inactive']) ?>

    <div class="form-group float-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>
