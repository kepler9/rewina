<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$this->title = 'Applicants';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="applicants-index table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'user.firstname',
                        'user.lastname',
                        'user.surname',
                        'user.email:email',
                        'user.phone',
                        ['attribute'=>'status','value'=>function($model){
                            return $model->status ? '<span class="badge badge-success">active</span>' : '<span class="badge badge-danger">inactive</span>';
                        },'format'=>'raw'],
                        ['attribute'=>'user.created_at','label'=>'Date','format'=>'date'],
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, backend\models\Applicants $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'applicant_id' => $model->applicant_id]);
                             },
                             'template' => '{view}',
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>