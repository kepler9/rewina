<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Create Users', ['create'], ['class' => 'btn btn-success']) ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="users-index">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        // 'user_id',
                        'surname',
                        'firstname',
                        'lastname',
                        'username',
                        //'auth_key',
                        //'password_hash',
                        //'password_reset_token',
                        'email:email',
                        'phone',
                        'status',
                        'created_at',
                        //'updated_at',
                        //'verification_token',
                        //'role',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, backend\models\Users $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'user_id' => $model->user_id]);
                             }
                        ],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>