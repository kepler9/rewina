<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model backend\models\ApplicationStatus */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Application Statuses', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>


<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item"><?= Html::a('Update', ['update', 'status_id' => $model->status_id], ['class' => 'btn btn-primary']) ?></li>
                    <li class="list-inline-item"><?= Html::a('Delete', ['delete', 'status_id' => $model->status_id], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="application-status-view">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'status_id',
                        'name',
                        'alert',
                        'description:ntext',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>