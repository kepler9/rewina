<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$this->title = 'Application Statuses';
$this->params['breadcrumbs'][] = $this->title;
?>


<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item"><?= Html::a('Create Application Status', ['create'], ['class' => 'btn btn-success']) ?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="application-status-index table-responsive">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],

                        'status_id',
                        'name',
                        'alert',
                        'description:ntext',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, backend\models\ApplicationStatus $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'status_id' => $model->status_id]);
                             },
                             'template'=>'{view}'
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>