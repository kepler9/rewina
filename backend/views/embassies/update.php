<?php
use yii\helpers\Html;
$this->title = 'Update Embassies: ' . $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Embassies', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'embassy_id' => $model->embassy_id]];
$this->params['breadcrumbs'][] = 'Update';
?>

<div class="page-wrapper">
	<div class="boxed">
		<div class="page-header bg-light">
			<div class="float-left">
				<h5><?=$this->title?></h5>
			</div>
			<div class="float-right">
				<ul class="list-inline">
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
				</ul>
			</div>
			<span class="clearfix"></span>
		</div>
		<div class="page-content">
			<div class="embassies-update">

			    <?= $this->render('_form', [
			        'model' => $model,
			        'user' => $model->user,
			    ]) ?>

			</div>
		</div>
	</div>
</div>