<?php
use yii\helpers\Html;
use yii\grid\GridView;
$this->title = 'Settings';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item"><?=Html::a('',['#']);?></li>
                    <li class="list-inline-item"><?= Html::a('Create Settings', ['create'], ['class' => 'btn btn-success']) ?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'name',
                        'value:ntext',
                        ['attribute'=>'created_at','format'=>'date','label'=>'Date Created'],

                        ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>