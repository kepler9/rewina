<div class="row">
    <div class="col-md-3">
        <div class="card-holder">
            <div class="card bg-blue row">
                <div onclick="window.location='/jobs/index'" class="card-content">
                    <h3>0</h3>
                    <p>Jobs</p>
                    <div class="card-icon">
                        <i class="fa fa-credit-card" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card-holder">
            <div class="card bg-black row">
                <div onclick="window.location='/applicants/index'" class="card-content">
                    <h3>0</h3>
                    <p>Applicants</p>
                    <div class="card-icon">
                        <i class="fa fa-user" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card-holder">
            <div class="card bg-danger row">
                <div onclick="window.location='/applications/index'" class="card-content">
                    <h3>0</h3>
                    <p>Applications</p>
                    <div class="card-icon">
                        <i class="fa fa-folder-open-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3">
        <div class="card-holder">
            <div class="card bg-info row">
                <div onclick="window.location='/companies/index'" class="card-content">
                    <h3>0</h3>
                    <p>Companies</p>
                    <div class="card-icon">
                        <i class="fa fa-folder-open-o" aria-hidden="true"></i>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>