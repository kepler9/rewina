<?php
	use yii\helpers\Html;
	$this->title = "Welcome";
	$this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-wrapper">
	<div class="boxed">
		<div class="page-header bg-light">
			<div class="float-left">
				<h5><?=$this->title?></h5>
			</div>
			<div class="float-right">
				<ul class="list-inline">
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
				</ul>
			</div>
			<span class="clearfix"></span>
		</div>
		<div class="page-content">
			<div class="container-fluid">
				<?=$this->render('_admin');?>
			</div>
		</div>
	</div>
</div>