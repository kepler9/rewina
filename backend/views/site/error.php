<?php
use yii\helpers\Html;
$this->title = $name;
?>
<div class="page-wrapper">
    <div style="<?= !$this->params['breadcrumbs'] ? 'margin-top: 50px;' : NULL ?>" class="boxed">
        <?php if(Yii::$app->user->identity):?>
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><b><?=$this->title?></b></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item"><?=Html::a('',['#']);?></li>
                    <li class="list-inline-item"><?=Html::a('',['#']);?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <?php endif;?>
        <div class="page-content">
            <div class="site-error">

                <?php if(!Yii::$app->user->identity):?>
                    <div class="form-header">
                        <h6><?= Html::encode($this->title) ?></h6>
                    </div>
                <?php endif;?>

                <div class="alert alert-danger">
                    <?= nl2br(Html::encode($message)) ?>
                </div>

                <p>
                    The above error occurred while the Web server was processing your request.
                </p>
                <p>
                    Please contact us if you think this is a server error. Thank you.
                </p>

            </div>
        </div>
    </div>
</div>