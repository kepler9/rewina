<?php
    use yii\helpers\Html;
    use yii\grid\GridView;
    use yii\widgets\ActiveForm;
    use yii\helpers\ArrayHelper;
    $this->title = 'Application Form';
    $this->params['breadcrumbs'][] = ['label'=>$this->title,'url'=>['index']];
?>
<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li></li>
                    <li class="list-inline-item"><?= Html::a('Create Form Fields', ['create'], ['class' => 'btn btn-success']) ?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <?php $form = ActiveForm::begin(['method'=>'post','options'=>['class'=>'form-inline']]);?>
                <?=$form->field($searchModel,'job_id')->dropDownList(ArrayHelper::map(\backend\models\Jobs::find()->all(),'job_id','title'),['prompt'=>'Select Job','onchange'=>'this.form.submit()'])->label(false);?>
            <?php ActiveForm::end();?>
            <br>
            <div class="form-fields-index table-responsive">
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],
                        'Label',
                        'Name',
                        'Type',
                        'Validation',
                        ['attribute'=>'Required','value'=>function($model){
                            return $model->Required ? "Yes" : "No";
                        }],
                        'Step',
                        ['attribute'=>'job.title','label'=>'Job'],
                        ['class' => 'yii\grid\ActionColumn','template'=>'{view}'],
                    ],
                ]); ?>
            </div>
        </div>
    </div>
</div>