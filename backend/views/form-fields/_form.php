<?php
	use yii\helpers\Html;
	use yii\helpers\ArrayHelper;
	use yii\widgets\ActiveForm;
?>
<div class="form-fields-form">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'Label')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'Name')->textInput(['maxlength' => true]) ?>
    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'Type')->dropDownList(['textbox' => 'Text Box','textarea'=>'Text Area','dropdown'=>'Drop Down','date'=>'Date','file'=>'File'],['prompt'=>'Select']) ?> 
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Validation')->dropDownList(['number'=>'Number','email'=>'Email','alphanumeric'=>'Alphanumeric','date'=>'Date'],['prompt'=>'Select']) ?> 
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Required')->dropDownList([1=>'Yes',0=>'No']) ?> 
        </div>
    </div> 
    <?= $form->field($model, 'Options')->textArea(['rows'=>3]) ?> 
    <div class="row">
        <div class="col-md-4">   
            <?= $form->field($model, 'job_id')->dropDownList(ArrayHelper::map(\backend\models\Jobs::find()->all(),'job_id','title'),['prompt'=>'Select'])->label('Job'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Ordering')->textInput()->label('Display Order'); ?>
        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'Step')->textInput()->label('Form Step'); ?>
        </div>
    </div>
    <label>Input Template</label>
    <?= $form->field($model,'Template')->fileInput(['name'=>'Template'])->label(false);?>
    <hr class="hr-20">
    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>
    <?php ActiveForm::end(); ?>
</div>