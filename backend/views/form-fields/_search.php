<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model admin\models\FormFieldsSearch */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-fields-search">

    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

    <?= $form->field($model, 'FormFieldID') ?>

    <?= $form->field($model, 'Type') ?>

    <?= $form->field($model, 'Name') ?>

    <?= $form->field($model, 'Label') ?>

    <?= $form->field($model, 'Options') ?>

    <?php // echo $form->field($model, 'Validation') ?>

    <?php // echo $form->field($model, 'Required') ?>

    <?php // echo $form->field($model, 'InsuranceProductCategoryID') ?>

    <?php // echo $form->field($model, 'Ordering') ?>

    <?php // echo $form->field($model, 'Step') ?>

    <?php // echo $form->field($model, 'Form') ?>

    <div class="form-group">
        <?= Html::submitButton('Search', ['class' => 'btn btn-primary']) ?>
        <?= Html::resetButton('Reset', ['class' => 'btn btn-outline-secondary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
