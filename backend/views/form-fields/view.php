<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->Label;
$this->params['breadcrumbs'][] = ['label' => 'Application Form', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Update', ['update', 'id' => $model->FormFieldID], ['class' => 'btn btn-primary']) ?>
                    </li>
                    <li class="list-inline-item">
                        <?= Html::a('Delete', ['delete', 'id' => $model->FormFieldID], [
                        'class' => 'btn btn-danger',
                        'data' => [
                            'confirm' => 'Are you sure you want to delete this item?',
                            'method' => 'post',
                        ],
                    ]) ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="form-fields-view">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        'Label',
                        'Name',
                        'Type',
                        'Validation',
                        ['attribute'=>'Required','value'=>function($model){
                            return $model->Required ? "Yes" : "No";
                        }],
                        'Options',
                        ['attribute'=>'insuranceProductCategory.Name','label'=>'Insurance Product Category'],
                        'Form',
                    ],
                ]) ?>

            </div>
        </div>
    </div>
</div>