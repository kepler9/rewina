<?php
	use yii\helpers\Html;
	$this->title = 'Update: ' . $model->Name;
	$this->params['breadcrumbs'][] = ['label' => 'Application Form', 'url' => ['index']];
	$this->params['breadcrumbs'][] = ['label' => $model->Label, 'url' => ['view', 'id' => $model->FormFieldID]];
	$this->params['breadcrumbs'][] = 'Update';
?>
<div class="page-wrapper">
	<div class="boxed">
		<div class="page-header bg-light">
			<div class="float-left">
				<h5><?=$this->title?></h5>
			</div>
			<div class="float-right">
				<ul class="list-inline">
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
					<li class="list-inline-item"><?=Html::a('',['#']);?></li>
				</ul>
			</div>
			<span class="clearfix"></span>
		</div>
		<div class="page-content">
			<div class="form-fields-update">

			    <?= $this->render('_form', [
			        'model' => $model,
			    ]) ?>

			</div>
		</div>
	</div>
</div>