<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
$this->title = $model->job->title;
$this->params['breadcrumbs'][] = ['label' => 'Applications', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
use yii\grid\GridView;
\yii\web\YiiAsset::register($this);
?>
<style type="text/css">.summary{display: none}</style>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                        <?= Html::a('Update', ['update', 'application_id' => $model->application_id], ['class' => 'btn btn-primary']) ?>
                    </li>
                    <li class="list-inline-item">
                        <?= Html::a('Delete', ['delete', 'application_id' => $model->application_id], [
                            'class' => 'btn btn-danger',
                            'data' => [
                                'confirm' => 'Are you sure you want to delete this item?',
                                'method' => 'post',
                            ],
                        ]) ?>
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="applications-view">

                <?= DetailView::widget([
                    'model' => $model,
                    'attributes' => [
                        ['attribute'=>'job.title','label'=>'Job'],
                        ['attribute'=>'applicant_id','value'=>function($model){
                            return Html::a($model->applicant->user->firstname." ".$model->applicant->user->lastname,['applicants/view','applicant_id'=>$model->applicant_id]);
                        },'label'=>'Applicant','format'=>'raw'],
                        ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                            return "<span data-toggle='tooltip' data-trigger='hover' data-placement='top' data-original-title='".$model->status->description."' class='badge badge-".$model->status->alert."'>".$model->status->name."</span>";
                        },'label'=>'Status'],
                        ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                            return $model->status_description ? $model->status_description : $model->status->description;
                        },'label'=>'Description'],
                        'date_applied',
                        ['attribute'=>'cv','label'=>'CV','format'=>'raw','value'=>function($model){
                            return '<a target="_blank" href="'.$this->context->settings->frontend_host.'/uploads/'.$model->cv.'">View</a>';
                        }],
                        ['attribute'=>'cover_letter','label'=>'Cover Letter','format'=>'raw','value'=>function($model){
                            return '<a target="_blank" href="'.$this->context->settings->frontend_host.'/uploads/'.$model->cover_letter.'">View</a>';
                        }],
                    ],
                ]) ?>

                <?= GridView::widget([
                    'dataProvider' => $FormFieldsData,
                    'showHeader'=> false,
                    'columns' => [
                        // ['class' => 'yii\grid\SerialColumn'],

                        ['attribute'=>'FormFieldID','value'=>function($model){
                            return "<b>".$model->formField->Label."</b>";
                        },'label'=>'Title','format'=>'raw'],
                        ['attribute'=>'Value','label'=>'Information','value'=>function($model){
                            if($model->formField->Type == 'file'){
                                return '<a target="_blank" href="'.$this->context->settings->frontend_host.'/uploads/'.$model->Value.'">Uploaded '.$model->formField->Name.'</a>';
                            }
                            return $model->Value;
                        },'format'=>'raw'],

                        // ['class' => 'yii\grid\ActionColumn'],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>