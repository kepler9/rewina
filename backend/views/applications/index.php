<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\grid\ActionColumn;
    use yii\grid\GridView;
    $this->title = 'Applications';
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item">
                    </li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="applications-index table-responsive">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        ['attribute'=>'job.title','label'=>'Job'],
                        ['attribute'=>'applicant_id','value'=>function($model){
                            return Html::a($model->applicant->user->firstname." ".$model->applicant->user->lastname,['applicants/view','applicant_id'=>$model->applicant_id]);
                        },'label'=>'Applicant','format'=>'raw'],
                        ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                            return "<span data-toggle='tooltip' data-trigger='hover' data-placement='top' data-original-title='".$model->status->description."' class='badge badge-".$model->status->alert."'>".$model->status->name."</span>";
                        },'label'=>'Status'],
                        ['attribute'=>'status_id','format'=>'raw','value'=>function($model){
                            return $model->status_description ? $model->status_description : $model->status->description;
                        },'label'=>'Description'],
                        'date_applied',
                        [
                            'class' => ActionColumn::className(),
                            'urlCreator' => function ($action, backend\models\Applications $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'application_id' => $model->application_id]);
                             },
                             'template'=>'{view}',
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>
