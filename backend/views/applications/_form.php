<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
?>

<div class="applications-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'status_id')->dropDownList(ArrayHelper::map(backend\models\ApplicationStatus::find()->all(),'status_id','name'),['prompt'=>'Select'])->label('Status') ?>

    <?= $form->field($model, 'status_description')->textarea(['rows' => 6]) ?>

    <div class="form-group pull-right">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>
    <span class="clearfix"></span>

    <?php ActiveForm::end(); ?>

</div>
