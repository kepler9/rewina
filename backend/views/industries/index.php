<?php
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;
$this->title = 'Industries';
$this->params['breadcrumbs'][] = $this->title;
?>

<div class="page-wrapper">
    <div class="boxed">
        <div class="page-header bg-light">
            <div class="float-left">
                <h5><?=$this->title?></h5>
            </div>
            <div class="float-right">
                <ul class="list-inline">
                    <li class="list-inline-item"><?= Html::a('Add Industries', ['create'], ['class' => 'btn btn-success']) ?></li>
                </ul>
            </div>
            <span class="clearfix"></span>
        </div>
        <div class="page-content">
            <div class="industries-index table-responsive">

                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    // 'filterModel' => $searchModel,
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        'name',
                        [
                            'class' => ActionColumn::className(),
                            'template' => '{view}',
                            'urlCreator' => function ($action, backend\models\Industries $model, $key, $index, $column) {
                                return Url::toRoute([$action, 'industry_id' => $model->industry_id]);
                             }
                        ],
                    ],
                ]); ?>

            </div>
        </div>
    </div>
</div>