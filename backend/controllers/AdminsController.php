<?php
namespace backend\controllers;
use backend\models\Users;
use backend\models\Admins;
use backend\models\AdminsSearch;
use backend\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use yii;
/**
 * AdminsController implements the CRUD actions for Admins model.
**/
class AdminsController extends MainController
{
    /**
     * @inheritDoc
    */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Admins models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new AdminsSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Admins model.
     * @param int $admin_id Admin ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($admin_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($admin_id),
        ]);
    }

    /**
     * Creates a new Admins model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Admins();
        $users = new Users();

        if ($this->request->isPost) {
            $users->load(Yii::$app->request->post());
            $user = new User();
            $password = Yii::$app->security->generateRandomString(8);
            $user->setPassword($password);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
            $user->role = 'admin';
            $user->firstname = $users->firstname;
            $user->lastname = $users->lastname;
            $user->email = $users->email;
            $user->phone = mb_substr($users->phone, -9);
            if($user->save()){
                $model->user_id = $user->user_id;
                if ($model->load($this->request->post()) && $model->save()) {
                    Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'adminAccount-html', 'text' => 'adminAccount-text'],
                            ['user' => $user,'password'=>$password]
                        )
                        ->setFrom([$this->settings->sender_email => $this->settings->sender_name])
                        ->setTo($user->email)
                        ->setSubject($this->settings->app_name.' Admin Account')
                        ->send();
                    return $this->redirect(['view', 'admin_id' => $model->admin_id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'user' => $users,
        ]);
    }

    /**
     * Updates an existing Admins model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $admin_id Admin ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($admin_id)
    {
        $model = $this->findModel($admin_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save() && $model->user->load($this->request->post()) && $model->user->save()) {
            return $this->redirect(['view', 'admin_id' => $model->admin_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Admins model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $admin_id Admin ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($admin_id)
    {
        $this->findModel($admin_id)->user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Admins model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $admin_id Admin ID
     * @return Admins the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($admin_id)
    {
        if (($model = Admins::findOne(['admin_id' => $admin_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
