<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use backend\models\Settings;
use backend\models\Methods;
/**
 * MainController
 */
class MainController extends Controller
{
    public $layout = 'admin';
    public $settings;
    public $method;
    /**
     * {@inheritdoc}
    */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
            // 'access' => [
            //     'class' => AccessControl::className(),
            //     'rules' => [
            //         [
            //             'allow' => true,
            //             'roles' => ['@'],
            //         ]
            //     ],
            // ],
        ];
    }
    public function beforeAction($action) {
        $this->method = new Methods();
        $this->settings = $this->method->get_system_settings();
        Yii::$app->session->set('settings',$this->settings);
        return parent::beforeAction($action);
    }
}
