<?php

namespace backend\controllers;
use backend\models\Users;
use backend\models\Embassies;
use backend\models\EmbassiesSearch;
use backend\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use common\models\User;
use yii;
/**
 * EmbassiesController implements the CRUD actions for Embassies model.
 */
class EmbassiesController extends MainController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Embassies models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new EmbassiesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Embassies model.
     * @param int $embassy_id Embassy ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($embassy_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($embassy_id),
        ]);
    }

    /**
     * Creates a new Embassies model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Embassies();
        $users = new Users();

        if ($this->request->isPost) {
            $users->load(Yii::$app->request->post());
            $user = new User();
            $password = Yii::$app->security->generateRandomString(8);
            $user->setPassword($password);
            $user->generateAuthKey();
            $user->generateEmailVerificationToken();
            $user->role = 'embassy';
            $user->firstname = $users->firstname;
            $user->lastname = $users->lastname;
            $user->email = $users->email;
            $user->phone = mb_substr($users->phone, -9);
            if($user->save()){
                $model->user_id = $user->user_id;
                if ($model->load($this->request->post()) && $model->save()) {
                    Yii::$app
                        ->mailer
                        ->compose(
                            ['html' => 'adminAccount-html', 'text' => 'adminAccount-text'],
                            ['user' => $user,'password'=>$password]
                        )
                        ->setFrom([$this->settings->sender_email => $this->settings->sender_name])
                        ->setTo($user->email)
                        ->setSubject($this->settings->app_name.' Embassy Account')
                        ->send();
                    return $this->redirect(['view', 'embassy_id' => $model->embassy_id]);
                }
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
            'user' => $users,
        ]);
    }

    /**
     * Updates an existing Embassies model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $embassy_id Embassy ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($embassy_id)
    {
        $model = $this->findModel($embassy_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'embassy_id' => $model->embassy_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Embassies model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $embassy_id Embassy ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($embassy_id)
    {
        $this->findModel($embassy_id)->user->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Embassies model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $embassy_id Embassy ID
     * @return Embassies the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($embassy_id)
    {
        if (($model = Embassies::findOne(['embassy_id' => $embassy_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
