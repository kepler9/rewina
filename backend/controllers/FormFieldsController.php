<?php
namespace backend\controllers;
use yii\web\UploadedFile;
use Yii;
use backend\models\FormFields;
use backend\models\FormFieldsSearch;
use backend\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * FormFieldsController implements the CRUD actions for FormFields model.
 */
class FormFieldsController extends MainController
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all FormFields models.
     * @return mixed
     */
    public function actionIndex($job_id=null,$Form=null) {
        $searchModel = new FormFieldsSearch();
        $searchModel->job_id = $job_id;
        if(Yii::$app->request->post()){
            $job_id = Yii::$app->request->post('FormFieldsSearch')['job_id'];
            $searchModel->job_id = $job_id;
        }
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single FormFields model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new FormFields model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new FormFields();
        
        if(Yii::$app->request->post()){
            if ($model->load(Yii::$app->request->post())) {
                if(isset($_FILES['Template']['name'])){
                    $FileUpload = new \backend\models\FileUpload();
                    $FileUpload->load(Yii::$app->request->post(),"");
                    $FileUpload->UploadFile = UploadedFile::getInstanceByName("Template");
                    if($FileUpload->UploadFile){
                        $model->Template = $FileUpload->upload();
                    }
                }
                if($model->save()){
                    return $this->redirect(['index', 'job_id' => $model->job_id]);
                }
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing FormFields model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if(Yii::$app->request->post()){
            if ($model->load(Yii::$app->request->post())) {
                if(isset($_FILES['Template']['name'])){
                    $FileUpload = new \backend\models\FileUpload();
                    $FileUpload->load(Yii::$app->request->post(),"");
                    $FileUpload->UploadFile = UploadedFile::getInstanceByName("Template");
                    if($FileUpload->UploadFile){
                        $model->Template = $FileUpload->upload();
                    }
                }
                if($model->save()){
                    return $this->redirect(['index', 'job_id' => $model->job_id]);
                }
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing FormFields model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the FormFields model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return FormFields the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = FormFields::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
