<?php
namespace backend\controllers;
use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;

class WelcomeController extends MainController {
    
    public function actionIndex() {
        return $this->render('index');
    }

    public function actionEmails(){
        return $this->render('../../mail/layouts/html');
    }

}