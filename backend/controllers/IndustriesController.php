<?php

namespace backend\controllers;

use backend\models\Industries;
use backend\models\IndustriesSearch;
use backend\controllers\MainController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * IndustriesController implements the CRUD actions for Industries model.
 */
class IndustriesController extends MainController
{
    /**
     * @inheritDoc
     */
    public function behaviors()
    {
        return array_merge(
            parent::behaviors(),
            [
                'verbs' => [
                    'class' => VerbFilter::className(),
                    'actions' => [
                        'delete' => ['POST'],
                    ],
                ],
            ]
        );
    }

    /**
     * Lists all Industries models.
     *
     * @return string
     */
    public function actionIndex()
    {
        $searchModel = new IndustriesSearch();
        $dataProvider = $searchModel->search($this->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Industries model.
     * @param int $industry_id Industry ID
     * @return string
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($industry_id)
    {
        return $this->render('view', [
            'model' => $this->findModel($industry_id),
        ]);
    }

    /**
     * Creates a new Industries model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return string|\yii\web\Response
     */
    public function actionCreate()
    {
        $model = new Industries();

        if ($this->request->isPost) {
            if ($model->load($this->request->post()) && $model->save()) {
                return $this->redirect(['view', 'industry_id' => $model->industry_id]);
            }
        } else {
            $model->loadDefaultValues();
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Industries model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param int $industry_id Industry ID
     * @return string|\yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($industry_id)
    {
        $model = $this->findModel($industry_id);

        if ($this->request->isPost && $model->load($this->request->post()) && $model->save()) {
            return $this->redirect(['view', 'industry_id' => $model->industry_id]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Industries model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param int $industry_id Industry ID
     * @return \yii\web\Response
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($industry_id)
    {
        $this->findModel($industry_id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Industries model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param int $industry_id Industry ID
     * @return Industries the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($industry_id)
    {
        if (($model = Industries::findOne(['industry_id' => $industry_id])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
