<?php
namespace backend\models;
use yii;
use yii\web\UploadedFile;
use yii\helpers\Json;
/**
** 
**/
class Methods extends \yii\base\Model
{
	public function get_system_settings(){
        $system_settings = Settings::find()->asArray()->all();
        $settings = array();
        foreach($system_settings as $key){
            $settings[$key['name']] = $key['value'];
        }
        return (object) $settings;
    }
}