<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "industries".
 *
 * @property int $industry_id
 * @property string $name
 *
 * @property Jobs[] $jobs
 */
class Industries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'industries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'industry_id' => 'Industry ID',
            'name' => 'Name',
        ];
    }

    /**
     * Gets query for [[Jobs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Jobs::className(), ['industry_id' => 'industry_id']);
    }
}
