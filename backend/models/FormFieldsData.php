<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "form_fields_data".
 *
 * @property int $FormFieldDataID
 * @property int $FormFieldID
 * @property string $Name
 * @property string $Value
 * @property int $user_id
 * @property int $application_id
 * @property int $job_id
 * @property int $Submitted
 * @property string $DateCreated
 *
 * @property Applications $application
 * @property FormFields $formField
 * @property Jobs $job
 * @property Users $user
 */
class FormFieldsData extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_fields_data';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FormFieldID', 'Name', 'Value', 'user_id', 'application_id', 'job_id', 'Submitted'], 'required'],
            [['FormFieldID', 'user_id', 'application_id', 'job_id', 'Submitted'], 'integer'],
            [['Value'], 'string'],
            [['DateCreated'], 'safe'],
            [['Name'], 'string', 'max' => 255],
            [['application_id'], 'exist', 'skipOnError' => true, 'targetClass' => Applications::className(), 'targetAttribute' => ['application_id' => 'application_id']],
            [['FormFieldID'], 'exist', 'skipOnError' => true, 'targetClass' => FormFields::className(), 'targetAttribute' => ['FormFieldID' => 'FormFieldID']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'job_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'FormFieldDataID' => 'Form Field Data ID',
            'FormFieldID' => 'Form Field ID',
            'Name' => 'Name',
            'Value' => 'Value',
            'user_id' => 'User ID',
            'application_id' => 'Application ID',
            'job_id' => 'Job ID',
            'Submitted' => 'Submitted',
            'DateCreated' => 'Date Created',
        ];
    }

    /**
     * Gets query for [[Application]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplication()
    {
        return $this->hasOne(Applications::className(), ['application_id' => 'application_id']);
    }

    /**
     * Gets query for [[FormField]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormField()
    {
        return $this->hasOne(FormFields::className(), ['FormFieldID' => 'FormFieldID']);
    }

    /**
     * Gets query for [[Job]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['job_id' => 'job_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }
}
