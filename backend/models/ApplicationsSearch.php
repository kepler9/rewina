<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\Applications;

/**
 * ApplicationsSearch represents the model behind the search form of `backend\models\Applications`.
 */
class ApplicationsSearch extends Applications
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['application_id', 'job_id', 'applicant_id', 'status_id'], 'integer'],
            [['status_description', 'date_applied'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Applications::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'application_id' => $this->application_id,
            'job_id' => $this->job_id,
            'applicant_id' => $this->applicant_id,
            'status_id' => $this->status_id,
            'date_applied' => $this->date_applied,
        ]);

        $query->andFilterWhere(['like', 'status_description', $this->status_description]);

        return $dataProvider;
    }
}
