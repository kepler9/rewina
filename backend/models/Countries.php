<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "countries".
 *
 * @property int $country_id
 * @property string $iso
 * @property string $name
 * @property string $nicename
 * @property string|null $iso3
 * @property int|null $numcode
 * @property string $phonecode
 * @property string|null $currency_code
 * @property string|null $currency_name
 *
 * @property Embassies[] $embassies
 * @property Jobs[] $jobs
 */
class Countries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'countries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['iso', 'name', 'nicename', 'phonecode'], 'required'],
            [['numcode'], 'integer'],
            [['iso'], 'string', 'max' => 2],
            [['name', 'nicename'], 'string', 'max' => 80],
            [['iso3'], 'string', 'max' => 3],
            [['phonecode'], 'string', 'max' => 6],
            [['currency_code', 'currency_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'country_id' => 'Country ID',
            'iso' => 'Iso',
            'name' => 'Name',
            'nicename' => 'Nicename',
            'iso3' => 'Iso3',
            'numcode' => 'Numcode',
            'phonecode' => 'Phonecode',
            'currency_code' => 'Currency Code',
            'currency_name' => 'Currency Name',
        ];
    }

    /**
     * Gets query for [[Embassies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmbassies()
    {
        return $this->hasMany(Embassies::className(), ['country_id' => 'country_id']);
    }

    /**
     * Gets query for [[Jobs]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJobs()
    {
        return $this->hasMany(Jobs::className(), ['country_id' => 'country_id']);
    }
}
