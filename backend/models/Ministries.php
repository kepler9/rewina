<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "ministries".
 *
 * @property int $ministry_id
 * @property int $country_id
 * @property string $name
 * @property int $user_id
 * @property int $status
 */
class Ministries extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ministries';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['country_id', 'name', 'user_id'], 'required'],
            [['country_id', 'user_id', 'status'], 'integer'],
            [['name'], 'string', 'max' => 500],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'ministry_id' => 'Ministry ID',
            'country_id' => 'Country ID',
            'name' => 'Name',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }
}
