<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "jobs".
 *
 * @property int $job_id
 * @property int $industry_id
 * @property string $title
 * @property string $description
 * @property string $requirements
 * @property string $responsibilities
 * @property string $seniority
 * @property int $country_id
 * @property int|null $company_id
 * @property string|null $deadline
 * @property string $date_created
 * @property int $status
 * @property int $positions
 *
 * @property Applications[] $applications
 * @property Companies $company
 * @property Countries $country
 * @property FormFields[] $formFields
 * @property FormFieldsData[] $formFieldsDatas
 * @property Industries $industry
 */
class Jobs extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'jobs';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['industry_id', 'title', 'description', 'requirements', 'responsibilities', 'seniority', 'country_id'], 'required'],
            [['industry_id', 'country_id', 'company_id', 'status', 'positions'], 'integer'],
            [['description', 'requirements', 'responsibilities'], 'string'],
            [['deadline', 'date_created'], 'safe'],
            [['title', 'seniority'], 'string', 'max' => 255],
            [['industry_id'], 'exist', 'skipOnError' => true, 'targetClass' => Industries::className(), 'targetAttribute' => ['industry_id' => 'industry_id']],
            [['country_id'], 'exist', 'skipOnError' => true, 'targetClass' => Countries::className(), 'targetAttribute' => ['country_id' => 'country_id']],
            [['company_id'], 'exist', 'skipOnError' => true, 'targetClass' => Companies::className(), 'targetAttribute' => ['company_id' => 'company_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'job_id' => 'Job ID',
            'industry_id' => 'Industry ID',
            'title' => 'Title',
            'description' => 'Description',
            'requirements' => 'Requirements',
            'responsibilities' => 'Responsibilities',
            'seniority' => 'Seniority',
            'country_id' => 'Country ID',
            'company_id' => 'Company ID',
            'deadline' => 'Deadline',
            'date_created' => 'Date Created',
            'status' => 'Status',
            'positions' => 'Positions',
        ];
    }

    /**
     * Gets query for [[Applications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['job_id' => 'job_id']);
    }

    /**
     * Gets query for [[Company]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompany()
    {
        return $this->hasOne(Companies::className(), ['company_id' => 'company_id']);
    }

    /**
     * Gets query for [[Country]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCountry()
    {
        return $this->hasOne(Countries::className(), ['country_id' => 'country_id']);
    }

    /**
     * Gets query for [[FormFields]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormFields()
    {
        return $this->hasMany(FormFields::className(), ['job_id' => 'job_id']);
    }

    /**
     * Gets query for [[FormFieldsDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormFieldsDatas()
    {
        return $this->hasMany(FormFieldsData::className(), ['job_id' => 'job_id']);
    }

    /**
     * Gets query for [[Industry]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getIndustry()
    {
        return $this->hasOne(Industries::className(), ['industry_id' => 'industry_id']);
    }
}
