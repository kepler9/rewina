<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "users".
 *
 * @property int $user_id
 * @property string $firstname
 * @property string|null $surname
 * @property string $lastname
 * @property string|null $username
 * @property string $auth_key
 * @property string $password_hash
 * @property string|null $password_reset_token
 * @property string $email
 * @property string $phone
 * @property int $status
 * @property int $created_at
 * @property int|null $updated_at
 * @property string|null $verification_token
 * @property string $role
 * @property string $avatar
 *
 * @property Admins[] $admins
 * @property Applicants[] $applicants
 * @property Companies[] $companies
 * @property Embassies[] $embassies
 * @property FormFieldsData[] $formFieldsDatas
 */
class Users extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'users';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['firstname', 'lastname', 'auth_key', 'password_hash', 'email', 'phone', 'created_at'], 'required'],
            [['status', 'created_at', 'updated_at'], 'integer'],
            [['firstname', 'surname', 'lastname', 'username', 'password_hash', 'password_reset_token', 'email', 'phone', 'verification_token', 'role', 'avatar'], 'string', 'max' => 255],
            [['auth_key'], 'string', 'max' => 32],
            [['email'], 'unique'],
            [['username'], 'unique'],
            [['password_reset_token'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => 'User ID',
            'firstname' => 'Firstname',
            'surname' => 'Surname',
            'lastname' => 'Lastname',
            'username' => 'Username',
            'auth_key' => 'Auth Key',
            'password_hash' => 'Password Hash',
            'password_reset_token' => 'Password Reset Token',
            'email' => 'Email',
            'phone' => 'Phone',
            'status' => 'Status',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
            'verification_token' => 'Verification Token',
            'role' => 'Role',
            'avatar' => 'Avatar',
        ];
    }

    /**
     * Gets query for [[Admins]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getAdmins()
    {
        return $this->hasMany(Admins::className(), ['user_id' => 'user_id']);
    }

    /**
     * Gets query for [[Applicants]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplicants()
    {
        return $this->hasMany(Applicants::className(), ['user_id' => 'user_id']);
    }

    /**
     * Gets query for [[Companies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCompanies()
    {
        return $this->hasMany(Companies::className(), ['user_id' => 'user_id']);
    }

    /**
     * Gets query for [[Embassies]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEmbassies()
    {
        return $this->hasMany(Embassies::className(), ['user_id' => 'user_id']);
    }

    /**
     * Gets query for [[FormFieldsDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormFieldsDatas()
    {
        return $this->hasMany(FormFieldsData::className(), ['user_id' => 'user_id']);
    }
}
