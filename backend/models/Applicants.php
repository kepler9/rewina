<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "applicants".
 *
 * @property int $applicant_id
 * @property int $user_id
 * @property int $status
 *
 * @property Applications[] $applications
 * @property FormFieldsData[] $formFieldsDatas
 * @property Users $user
 */
class Applicants extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applicants';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'status'], 'integer'],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => Users::className(), 'targetAttribute' => ['user_id' => 'user_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'applicant_id' => 'Applicant ID',
            'user_id' => 'User ID',
            'status' => 'Status',
        ];
    }

    /**
     * Gets query for [[Applications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['applicant_id' => 'applicant_id']);
    }

    /**
     * Gets query for [[FormFieldsDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormFieldsDatas()
    {
        return $this->hasMany(FormFieldsData::className(), ['applicant_id' => 'applicant_id']);
    }

    /**
     * Gets query for [[User]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(Users::className(), ['user_id' => 'user_id']);
    }
}
