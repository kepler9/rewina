<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "MailQueue".
 *
 * @property int $id
 * @property string|null $sender_name
 * @property string|null $sender_email
 * @property string|null $email_to
 * @property string|null $cc
 * @property string|null $bcc
 * @property string|null $subject
 * @property string|null $html_body
 * @property string|null $text_body
 * @property string|null $reply_to
 * @property string|null $charset
 * @property string $created_at
 * @property int|null $attempts
 * @property string|null $last_attempt_time
 * @property string|null $sent_time
 * @property string|null $time_to_send
 * @property string|null $swift_message
 * @property string|null $attachments
 * @property int $sent
 * @property string $status
 * @property string $email_template
 */
class MailQueue extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mail_queue';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['html_body', 'text_body', 'swift_message', 'attachments'], 'string'],
            [['created_at', 'last_attempt_time', 'sent_time', 'time_to_send'], 'safe'],
            [['attempts', 'sent'], 'integer'],
            [['sender_name', 'sender_email', 'email_to', 'cc', 'bcc', 'subject', 'reply_to', 'charset', 'email_template'], 'string', 'max' => 255],
            [['status'], 'string', 'max' => 20],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'sender_name' => 'Sender Name',
            'sender_email' => 'Sender Email',
            'email_to' => 'Email To',
            'cc' => 'Cc',
            'bcc' => 'Bcc',
            'subject' => 'Subject',
            'html_body' => 'Body',
            'text_body' => 'Text Body',
            'reply_to' => 'Reply To',
            'charset' => 'Charset',
            'created_at' => 'Date Created',
            'attempts' => 'Attempts',
            'last_attempt_time' => 'Last Attempt Time',
            'sent_time' => 'Sent Time',
            'time_to_send' => 'Time To Send',
            'swift_message' => 'Swift Message',
            'attachments' => 'Attachments',
            'sent' => 'Sent',
            'status' => 'Status',
            'email_template' => 'Email Template',
        ];
    }
}
