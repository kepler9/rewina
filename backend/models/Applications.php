<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "applications".
 *
 * @property int $application_id
 * @property int $job_id
 * @property int $applicant_id
 * @property int $status_id
 * @property string|null $status_description
 * @property string $cv
 * @property string $cover_letter
 * @property string $date_applied
 *
 * @property Applicants $applicant
 * @property FormFieldsData[] $formFieldsDatas
 * @property Jobs $job
 * @property ApplicationStatus $status
 */
class Applications extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'applications';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['job_id', 'applicant_id', 'status_id', 'cv', 'cover_letter'], 'required'],
            [['job_id', 'applicant_id', 'status_id'], 'integer'],
            [['status_description'], 'string'],
            [['date_applied'], 'safe'],
            [['cv', 'cover_letter'], 'string', 'max' => 500],
            [['applicant_id'], 'exist', 'skipOnError' => true, 'targetClass' => Applicants::className(), 'targetAttribute' => ['applicant_id' => 'applicant_id']],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'job_id']],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => ApplicationStatus::className(), 'targetAttribute' => ['status_id' => 'status_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'application_id' => 'Application ID',
            'job_id' => 'Job ID',
            'applicant_id' => 'Applicant ID',
            'status_id' => 'Status ID',
            'status_description' => 'Status Description',
            'cv' => 'Cv',
            'cover_letter' => 'Cover Letter',
            'date_applied' => 'Date Applied',
        ];
    }

    /**
     * Gets query for [[Applicant]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplicant()
    {
        return $this->hasOne(Applicants::className(), ['applicant_id' => 'applicant_id']);
    }

    /**
     * Gets query for [[FormFieldsDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormFieldsDatas()
    {
        return $this->hasMany(FormFieldsData::className(), ['application_id' => 'application_id']);
    }

    /**
     * Gets query for [[Job]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['job_id' => 'job_id']);
    }

    /**
     * Gets query for [[Status]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(ApplicationStatus::className(), ['status_id' => 'status_id']);
    }
}
