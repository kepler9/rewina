<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "form_fields".
 *
 * @property int $FormFieldID
 * @property string $Type
 * @property string $Name
 * @property string $Label
 * @property string|null $Options
 * @property string|null $Validation
 * @property int|null $Required
 * @property int $job_id
 * @property float|null $Ordering
 * @property int $Step
 * @property string|null $Form
 * @property string|null $Template
 *
 * @property FormFieldsData[] $formFieldsDatas
 * @property Jobs $job
 */
class FormFields extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'form_fields';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['Type', 'Name', 'Label', 'job_id', 'Step'], 'required'],
            [['Options', 'Template'], 'string'],
            [['Required', 'job_id', 'Step'], 'integer'],
            [['Ordering'], 'number'],
            [['Type', 'Name', 'Label', 'Validation'], 'string', 'max' => 255],
            [['Form'], 'string', 'max' => 50],
            [['job_id'], 'exist', 'skipOnError' => true, 'targetClass' => Jobs::className(), 'targetAttribute' => ['job_id' => 'job_id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'FormFieldID' => 'Form Field ID',
            'Type' => 'Type',
            'Name' => 'Name',
            'Label' => 'Label',
            'Options' => 'Options',
            'Validation' => 'Validation',
            'Required' => 'Required',
            'job_id' => 'Job ID',
            'Ordering' => 'Ordering',
            'Step' => 'Step',
            'Form' => 'Form',
            'Template' => 'Template',
        ];
    }

    /**
     * Gets query for [[FormFieldsDatas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getFormFieldsDatas()
    {
        return $this->hasMany(FormFieldsData::className(), ['FormFieldID' => 'FormFieldID']);
    }

    /**
     * Gets query for [[Job]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getJob()
    {
        return $this->hasOne(Jobs::className(), ['job_id' => 'job_id']);
    }
}
