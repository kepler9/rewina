<?php

namespace backend\models;

use Yii;

/**
 * This is the model class for table "application_status".
 *
 * @property int $status_id
 * @property string $name
 * @property string $alert
 * @property string $description
 *
 * @property Applications[] $applications
 */
class ApplicationStatus extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'application_status';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'alert', 'description'], 'required'],
            [['description'], 'string'],
            [['name', 'alert'], 'string', 'max' => 255],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'status_id' => 'Status ID',
            'name' => 'Name',
            'alert' => 'Alert',
            'description' => 'Description',
        ];
    }

    /**
     * Gets query for [[Applications]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getApplications()
    {
        return $this->hasMany(Applications::className(), ['status_id' => 'status_id']);
    }
}
