<?php

namespace backend\models;

use yii\base\Model;
use yii\data\ActiveDataProvider;
use backend\models\FormFieldsData;

/**
 * FormFieldsDataSearch represents the model behind the search form of `backend\models\FormFieldsData`.
 */
class FormFieldsDataSearch extends FormFieldsData
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['FormFieldDataID', 'FormFieldID', 'user_id', 'application_id', 'job_id', 'Submitted'], 'integer'],
            [['Name', 'Value', 'DateCreated'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = FormFieldsData::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'FormFieldDataID' => $this->FormFieldDataID,
            'FormFieldID' => $this->FormFieldID,
            'user_id' => $this->user_id,
            'application_id' => $this->application_id,
            'job_id' => $this->job_id,
            'Submitted' => $this->Submitted,
            'DateCreated' => $this->DateCreated,
        ]);

        $query->andFilterWhere(['like', 'Name', $this->Name])
            ->andFilterWhere(['like', 'Value', $this->Value]);

        return $dataProvider;
    }
}
