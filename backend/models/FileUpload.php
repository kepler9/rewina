<?php
namespace backend\models;
use Yii;
use yii\base\Model;
/**
 * file upload model
*/
class FileUpload extends Model {
    public $UploadFile;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['UploadFile'], 'required'],
            [['UploadFile'], 'file', 'skipOnEmpty' => false, 'extensions' => 'pdf,png,jpeg,doc,docx,jpg'],
        ];
    }
    public function upload() {
        if ($this->validate()) {
            $FileName = rand(100,1000)."-".str_replace(' ', '-', $this->UploadFile->baseName) . '.' . $this->UploadFile->extension;
            if($this->UploadFile->saveAs('uploads/' . $FileName)){
                return $FileName;
            }
        }
        return false;
    }
}
