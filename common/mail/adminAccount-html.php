<?php
	use yii\helpers\Html;
	$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
<div class="verify-email">
    <p>Hello <?= Html::encode($user->firstname) ?>,</p>

    <p>Your admin account has been created.</p>

    <p> Use this password to login:<b><?=$password?></b></p>

    <p>Follow the link below to activate your account:</p>

    <p><?= Html::a(Html::encode($verifyLink), $verifyLink) ?></p>
</div>