<?php
$verifyLink = Yii::$app->urlManager->createAbsoluteUrl(['site/verify-email', 'token' => $user->verification_token]);
?>
Hello <?= $user->firstname ?>,

Your admin account has been created.

Use this password to login:<?=$password?>

Follow the link below to activate your account:

<?= $verifyLink ?>
